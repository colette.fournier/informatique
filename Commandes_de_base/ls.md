
# NOM

	  ls	(list)

# FONCTION

La commande _ls_ liste le contenu des *répertoires* 

# UTILISATION

Il faut indiquer le _chemin du répertoire choisi_

	1°cas: _ Lister le répertoire courant_(celui qui serait donné par la commande pwd)

Pour lire simplement les *sous-répertoires et fichiers* de ce répertoire courant:

	ls

Pour les lire avec des informations sur chaque élément de ce répertoire:

	ls -l

Pour afficher la taille des éléments de façon lisible

	ls -lh

	2° cas: _ Lister un répertoire distant_

Supposons que le chemin du répertoire choisi soit _../../Racine/_ et son nom _toto_les 3 commandes précédentes deviennent:

	ls ../../Racine/toto

	ls -l ../../Racine/toto
  
  	ls -lh ../../Racine/toto



