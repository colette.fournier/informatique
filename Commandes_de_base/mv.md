##NOM

		mv	(mouvment)


##FONCTION

		La commande mv permet soit de changer le nom d'un ficher ou celui d'un répertoire
				      soit de changer le répertoire auquel appartient le fichier choisi ou celui auquel appartient
				      le sous-répertoire choisi


##UTILISATION

		On considère qu'on a suivi sur l'arbre le chemin suivant:

			/.../Racine/RepA/RepB/		RepB/ est le répertoire courant		

							/.../Racine/RepA/RepB/ s'appelle "CHEMIN ABSOLU" du répertoire RepB/

							Ce chemin nous a conduit devant RepB/

			fichx.doc est dans RepB/


		1° Changement de nom du fichier fichx.doc en fichy.doc sans changer le sous-répertoire auquel il appartient (ici RepB/)

			mv  ./fichx.doc  ./fichy.doc			./ (Répertoire courant) peut ne pas être écrit, on aura:

			mv  fichx.doc  fichy.doc		

		2° Changement de répertoire dans lequel se situe fichx.doc		

		
			fichx.doc est actuellement dans RepB/. 


			On veut que fichx.doc soit un fichier de Repb/		Repb/ est un sous-répertoire de Repa/ qui lui même est un
									        sous-répertoire de Racine/
									       
										Il faut donc revenir à Racine/ et suivre la branche qui
									        nous conduira à Repb/
										

			mv  fichx.doc  ../../Repa/Repb/				../../Repa/Repb/ s'appelle "CHEMIN RELATIF" du répertoire
									        qui a pour "NOM" Repb/
											

		3° Changement de nom d'un répertoire

			On veut changer le nom de RepB/ en RepK/

			On est toujours dans le cas où on a suivi le chemin absolu de RepB/ qui est /.../Racine/RepA/RepB/ et qui nous a
			conduit devant RepB/

			Il faut donc se placer devant RepA/ pour pouvoir changer le nom du sous répertoire RepB/ de RepA/ en RepK/

			cd  ../				

 			mv  ./RepB/  ./RepK/		( ou	 mv  RepB/  RepK/ )  


							Le chemin absolu de RepK/ est alors 	/.../Racine/RepA/RepK/


		4° Changement de chemin d'un répertoire

			On veut que RepB/ qui est un sous-répertoire de RepA/ soit un sous-répertoire de Racine/

			mv  /.../Racine/RepA/RepB/  /.../Racine/RepB/ 	 	Le changement de chemin du Répertoire RepB/ a été réalisé
									        ici avec l'écriture des chemins absolus. 

										Il peut être écrit aussi à l'aide des chemins relatifs.
							 			Cette possibilité dépend du répertoire courant du départ.
	





				



