---
title  COMMANDE *pwd*
...




#FONCTION
pwd pour present working directory
Cette commande donnera le 
##Notion de *Chemin*

Il s'agit du trajet parcouru sur l'arbre, depuis le départ de l'arbre jusqu'au répertoire où on est actuellement.

Supposons qu'on appelle Racine le *répertoire* d'où on _part_ sur l'arbre. Ce **répertoire** a un sous-répertoire Rep 1 qui
lui-même a un sous-répertoire Rep 2

Si, à l'aide de commandes à voir plus loin, on est parti de Racine et arrivé à Rep 2, la commande pwd donnera:
/...Racine/Rep 1/Rep 2/

(_/_ est le véritable départ de l'arbre, les 3 points suivants représentent des répertoires sans intéret pour le moment)

/...Racine/Rep 1/Rep 2/ est *le chemin* du Répertoire devant lequel on se trouve.

Ce Répertoire est le *répertoire courant*, Rep 2 dans l'exemple précédent.

##Symboles du répertoire courant et de son antécédent immédiat 

 En reprenant l'exemple pécédent: 

 Le répertoire courant, ici Rep 2, est représenté par ./

 Le répertoire directement immédiat à Rep 2, ici Rep 1, est représenté par ../

 Le répertoire directement immédiat à Rep 1, ici Racine,est représenté par ../../  
	
#UTILISATION

 La commande pwd s'utilise seule
 Le chemin du Répertoire courant qu'on obtiendra sera une chaîne de Répertoires.


