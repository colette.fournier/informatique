##NOM

	cd (change directory)

	
##FONCTION


La commande cd permet de _changer de répertoire_


##UTILISATION

On se déplace sur l'arbre jusqu'au répertoire voulu _en suivant son chemin_ depuis l'endroit où on est (donné par la commande pwd)

Ce _chemin doit être écrit après la commande cd_s'il est nécessaire de l'écrire depuis le départ de l'arbre

ou de le compléter à partir de l'endroit où on se trouve. 

En reprenant l'exemple choisi dans le texte sur la commande pwd, le chemin du répertoire courant est /...Racine/Rep1/Rep2/

(Rappel: les 3 points après le */* de départ représentent des répertoires dont on ne parle parle pas ici)

On sera donc dans les _2 cas suivants_ dans le Répertoire Rep2

1° cas: on veut aller dans un sous-répertoire Rep3 de Rep2 puis de là dans un sous répertoire Rep4 de Rep3

	L'écriture de cd sera *cd Rep3/Rep4/* 		Il n'est pas nécessaire d'écrire ici un chemin à suivre

						 	puisqu'on est dans Rep2.

2° cas: On veut atteindre un autre sous-répertoire Repa de Rep1 puis un sous-répertoire Repb de Repa _on doit revenir_ à Rep1

	L'écriture de cd sera *cd ../Repa/Repb/*    	*../* est l'écriture d'une remontée de 1 niveau puisqu'on part de Rep2

							et qu'on doit être dans Rep1 pour rétablir le chemin.

	De même, si on veut atteindre un autre sous-répertoire RepA de Racine puis un sous-répertoire RepB de RepA

	en partant toujours de Rep2,_on doit remonter de 2 niveaux_pour se retrouver dans Racine et rétablir le chemin

	de RepA.

L écriture de cd sera *cd ../../RepA/RepB/*
