---
title: 'PREAMBULE A L''ORGANISATION D''UN CLASSEMENT'
...

# ORGANISATION D'UN CLASSEMENT

## Notion d'arbre
						
Un *classement* s'organise sous la forme d'un arbre (analogie avec un arbre généalogique).

Le départ de cet arbre est représenté par un seul slash (*/*).
De ce / part une première *branche* qui donne naissance à une ou plusieurs autres branches etc.

Le point d'attache d'une branche à une autre est appelé un *noeud*

Chacune de ces branches conduit

- à *1 fichier*

- ou à *1 répertoire* qui pourra contenir à son tour à la fois des fichiers ou des répertoires.

## Fichiers et répertoires

Les fichiers sont des documents qui ne peuvent conduire à rien d'autre. L'image d'un fichier pourrait
être une *feuille* de l'arbre.
Un répertoire est un *noeud* de l'arbre. Ce répertoire peut conduire à d'autres répertoires qui seront
des *sous-répertoires* du précédent. Il peut aussi conduire à des "feuilles" qui sont des *fichiers*

Si RepK est un sous-répertoire de Rep1, RepK pourrait aussi conduire à des fichiers et des sous-répertoires de RepK.

Dans un classement les noms des fichiers et répertoires sont écrits *sans intervalle*
Ex: Si on décide d'appeler un répertoire "Affaire GIRARD George", on peut écrire ce nom

- soit *AffaireGirardGeorge*

- soit *AffaireGIRARDGeorge*

- soit *Affaire\_GIRARD\_George*

- soit d'autres combinaisons de ce type                  
			
De plus	ces noms ne doivent contenir ni virgule, ni point, ni accent etc. donc ils ne contiennent que des lettres
 minuscules ou majuscules et des _.

*Complément dans l'éciture d'un fichier*
A l'exception des commandes dont on parlera plus loin, le nom de chaque fichier a une *extension* du genre .doc, .md ou .pdf.   
Exemple: AffaireGIRARDGeorge.pdf 

Ces extensions permettent de savoir quelle *commande* utiliser pour lire le fichier.

# COMMANDES DE BASE

Lorsqu'on se trouve en présence d'un arbre on dispose de *commandes* qui permettent de

- _Gérer_ fichiers et répertoires existants.

    - *pwd* pour connaître le Répertoire dans lequel on se trouve sur l'arbre.

    - *ls* pour lister le contenu d'un Répertoire.

    - *cd* pour changer de Répertoire.

    - *mv* pour déplacer un fichier ou répertoire dans un autre répertoire ou changer leur nom.

- _Créer_ un nouveau Répertoire.

    - *mkdir*

* _Touver le chemin_ qu'on doit suivre pour trouver un "fichier ou un répertoire perdu".

    - *find*

Toutes ces commandes
 
- sont écrites en minuscules

- sont des fichiers sans extension

- sont utilisées suivies d'un "blanc" si nécessaire.


Pour chacune il y aura *une page man* qui expliquera son utilisation.

# OUTILS de CREATION D'UN FICHIER TEXTE 

Ces "OUTILS" sont aussi des "COMMANDES" avec les mêmes propriétés que les précédentes 
mais elles permettent d'*écrire des fichiers-textes*, de les *éditer* et de les *lire*

- _Ecrire_ un fichier à un format choisi parmis plusieurs. 
Il a été choisi ici le fomat "Mark Down" d'où l'extension du fichier .md

*nano*	Après le "blanc" sera écrit le nom du fichier qu'on veut écrire.
Exemple: _nano FacturesEntretienAppart2019.md_ .
Ne pas oublier l'extension .md dans nano

- _Editer une page man_ de ce fichier

Pour le moment le fichier écrit est seulement dans l'ordinateur du créateur. 

*mkman* Permet de transmettre ce fichier au "système" de cet ordinateur.
Exemple: _mkman FacturesEntretienAppart2019_.Il ne faut pas écrire l'extension.

*man 9*	Le numéro 9 est la "page" du "manuel" choisie ici pour l'écriture.
Exemple: _man 9 FacturesEntretienAppart2019_. Permet de "lire" la "page man de FacturesEntretienAppart2019",
interprétée au format "Mark Down". Ne pas écrire ici non plus l'extension.

- _Editer le fichier en .pdf_ .Jusqu'à pésent le fichier a l'extension .md.

*mkpdf*	Transforme le fichier écrit en .md avec "nano" en fichier en .pdf. Exemple: mkpdf FacturesEntretienAppart2019.pdf.

*evince* Permet de lire ce fichier.pdf

